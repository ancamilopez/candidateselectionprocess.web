# Bienvenido a Proyecto Angular de Gestión de Candidatos y Experiencias para Pandapé

Este proyecto desarrollado en Angular se ha creado específicamente para satisfacer las necesidades de Pandapé, una empresa comprometida en la selección y administración de candidatos. La aplicación se encarga de la gestión integral de candidatos y sus experiencias laborales.

### Características Clave:

Creación de Candidatos: El sistema permite a los usuarios de Pandapé crear y registrar nuevos candidatos de manera eficiente. Los campos relevantes, como nombre, experiencia previa y habilidades, se capturan de forma organizada.

Gestión de Experiencias: Una de las características fundamentales de la aplicación es la posibilidad de agregar y administrar las experiencias laborales de cada candidato. Esto incluye detalles sobre las empresas anteriores, fechas de empleo, roles desempeñados y responsabilidades.

Consulta de Candidatos: La plataforma proporciona una interfaz intuitiva para buscar y acceder a la lista de candidatos registrados. Los usuarios pueden filtrar y ordenar los resultados según diferentes criterios, lo que facilita la localización de candidatos específicos.

Visualización de Experiencias: Los datos detallados sobre las experiencias laborales de cada candidato están disponibles para su revisión. Esto permite a los usuarios de Pandapé obtener una visión completa del historial de empleo de cada candidato.



## Instalación

### Requerimientos

- Tener una versión de Node mayor o igual a v18.13.0
- Tener una versión de Angular Cli igual o superior a v16.1.6

### Clonar proyecto

Clone el proyecto en su maquina  por medio de la URl : *https://gitlab.com/ancamilopez/candidateselectionprocess.web.git* o si lo desea por medio de SSH

```sh
git clone https://gitlab.com/ancamilopez/candidateselectionprocess.web.git <NombreEnLocal> -b develop

```

## Instalar

Después de estar en la carpeta de tu proyecto ejecuta el comando

```sh

cd <NombreEnLocal>
npm install

```

En la carpeta `environments` Encontrara dos archivos `environment.prod.ts` y `environment.ts` en los cuales encontrara las variables de entorno para configurar el API. 


```sh
apiUrl='https://localhost:<PUERTO_API>/api/'
```  

Una vez instalados los paquetes y configurado variables de entorno, ejecuta el comando 

```sh
ng serve
```  

Navega a http://localhost:4200/. La aplicación se recargará automáticamente si cambias alguno de los archivos fuente.

