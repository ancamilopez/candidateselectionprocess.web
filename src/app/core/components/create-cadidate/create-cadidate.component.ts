import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CoreService } from '../../core.service';
import { NotificationService } from '../../../shared/services/notification.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-create-cadidate',
  templateUrl: './create-cadidate.component.html',
  styleUrls: ['./create-cadidate.component.scss']
})
export class CreateCadidateComponent {

  form: FormGroup;
  experiencesList: any[] = [];
  constructor(private fb: FormBuilder, private _coreService: CoreService, private _notificationService: NotificationService) { }

  ngOnInit(): void {
    this.createForm();
  }

  private createForm() {
    this.form = this.fb.group({
      name: ['', Validators.required],
      surname: ['', Validators.required],
      birthdate: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]]
    });
  }

  onExperienceAdded(experiences: any[]) {
    this.experiencesList = experiences;
  }

  onSubmit() { 
    this._coreService.CreateCandidateWithExperiences(this.form.value, this.experiencesList).subscribe((res: any) => {
      var a = res;
      console.log(a);
    });
  }
}
