import { Component, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CoreService } from '../../core.service';
import { NotificationService } from '../../../shared/services/notification.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-list-add-experience',
  templateUrl: './list-add-experience.component.html',
  styleUrls: ['./list-add-experience.component.scss']
})
export class ListAddExperienceComponent {

  @Output() experiencesListAdd = new EventEmitter<any[]>();

  form: FormGroup;
  experiencesList: any[] = [];

  constructor(private fb: FormBuilder, private _coreService: CoreService, private _notificationService: NotificationService) { }

  ngOnInit(): void {
    this.createForm();
  }

  private createForm() { 
    this.form = this.fb.group({
      company: ['', Validators.required],
      job: ['', Validators.required],
      description: [''],
      salary: [null, Validators.min(0)],
      beginDate: [''],
      endDate: ['']
    });
  }

  addExperience() {
    if (this.form.valid) {
      this.experiencesList.push(this.form.value);
      this.experiencesList = [...this.experiencesList];
      this.experiencesListAdd.emit(this.experiencesList);
      this.form.reset();
    }
  }
}
