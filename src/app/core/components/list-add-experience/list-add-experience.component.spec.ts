import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAddExperienceComponent } from './list-add-experience.component';

describe('ListAddExperienceComponent', () => {
  let component: ListAddExperienceComponent;
  let fixture: ComponentFixture<ListAddExperienceComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ListAddExperienceComponent]
    });
    fixture = TestBed.createComponent(ListAddExperienceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
