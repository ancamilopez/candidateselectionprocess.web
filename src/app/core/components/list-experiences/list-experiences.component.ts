import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-list-experiences',
  templateUrl: './list-experiences.component.html',
  styleUrls: ['./list-experiences.component.scss']
})
export class ListExperiencesComponent {
  @Input() experienceList: any[] = [];
}
