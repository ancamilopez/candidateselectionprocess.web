import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCadidateComponent } from './list-cadidate.component';

describe('ListCadidateComponent', () => {
  let component: ListCadidateComponent;
  let fixture: ComponentFixture<ListCadidateComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ListCadidateComponent]
    });
    fixture = TestBed.createComponent(ListCadidateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
