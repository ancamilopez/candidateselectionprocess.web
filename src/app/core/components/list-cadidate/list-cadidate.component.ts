import { Component } from '@angular/core';
import { CoreService } from '../../core.service';
import { NotificationService } from '../../../shared/services/notification.service';
@Component({
  selector: 'app-list-cadidate',
  templateUrl: './list-cadidate.component.html',
  styleUrls: ['./list-cadidate.component.scss']
})
export class ListCadidateComponent {
  listCandidates: any[] = [];
  experiencesList: any[] = [];
  constructor(private _coreService: CoreService, private _notificationService: NotificationService) { }

  ngOnInit(): void {
    this.GetCandidates();
  }

  GetCandidates() {
    this._coreService.GetAllCandidates().subscribe((res: any) => {
      if(res) {
        this.listCandidates = res;
      }
    });
  }

  VerExperiencia(candidate: any) {
    this._coreService.GetAllCandidateExperiencesByIdCadidate(candidate.id).subscribe((res: any) => {
      if(res) {
        this.experiencesList = res.candidateExperiencesDto;
        this.experiencesList = [...this.experiencesList];
      }
    });
  }
}
