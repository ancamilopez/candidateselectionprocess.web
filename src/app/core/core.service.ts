import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({ providedIn: 'root' })
export class CoreService {
  constructor(private http: HttpClient) { }


  public CreateCandidateWithExperiences(candidate: any, experiences: any[]) {
    if (experiences.length == 0) {
      let send = {
        "candidate": candidate
      }
      return this.http.post(`${environment.apiUrl}Candidates`, send);
    } else {
      candidate.candidateExperiencesRequestDto = experiences;
      let send = {
        "candidate": candidate
      }

      return this.http.post(`${environment.apiUrl}Candidates/CreateCandidateWithExperiences`, send);
    }
  }

  public GetAllCandidates() {
    return this.http.get(`${environment.apiUrl}Candidates`);
  }

  public GetCandidateById(id: number) {
    return this.http.get(`${environment.apiUrl}Candidates/id?id=${id}`);
  }

  public GetAllCandidateExperiencesByIdCadidate(id: number) {
    return this.http.get(`${environment.apiUrl}Candidates/GetCandidateWithExperiences/${id}`);
  }

}
