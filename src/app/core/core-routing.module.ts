import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LayoutComponent } from './layout/layout.component';
import { CandidateComponent } from './pages/candidate/candidate.component'
import { ListCandidatesComponent } from './pages/list-candidates/list-candidates.component';


const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'candidate',
        component: CandidateComponent,
      } ,
      {
        path: 'candidates-list',
        component: ListCandidatesComponent,
      }
    ],
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CoreRoutingModule { }