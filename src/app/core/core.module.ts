import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';



import { CoreRoutingModule } from './core-routing.module';

import { CreateCadidateComponent } from './components/create-cadidate/create-cadidate.component';
import { ListCadidateComponent } from './components/list-cadidate/list-cadidate.component';
import { CandidateComponent } from './pages/candidate/candidate.component';
import { LayoutComponent } from './layout/layout.component';
import { ListAddExperienceComponent } from './components/list-add-experience/list-add-experience.component';
import { ListExperiencesComponent } from './components/list-experiences/list-experiences.component';
import { ListCandidatesComponent } from './pages/list-candidates/list-candidates.component';


@NgModule({
  declarations: [
    CreateCadidateComponent,
    ListCadidateComponent,
    LayoutComponent,
    CandidateComponent,
    ListAddExperienceComponent,
    ListExperiencesComponent,
    ListCandidatesComponent
  ],
  imports: [
    CommonModule,
    CoreRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,

  ]
})
export class CoreModule { }
